let pageIsRendering = false;
const scale = 2;

//Reder the page
const renderPage = async (num, canvas, PDFDOC) => {
  pageIsRendering = true;

  PDFDOC.getPage(num).then((page) => {
    let newContext = canvas.getContext("2d");
    const viewport = page.getViewport({ scale });
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    const renderContext = {
      canvasContext: newContext,
      viewport,
    };

    page.render(renderContext).promise.then(() => {
      pageIsRendering = false;
    });
  });
};

//Get document
window.addEventListener("load", function () {
  iniciar();
});
async function iniciar() {
  let URLS = document.querySelectorAll("canvas[id='pdf-render']");
  try {
    for (const canvas of URLS) {
      // Article element
      let article = canvas.closest("article");
      article.classList.add("canvas-article");
      // Canvas element
      canvas.classList.add("pdf-center");
      // Head element
      let head = document.createElement("div");
      head.classList.add("canvas-header");
      // Footer element
      let foot = document.createElement("div");
      foot.classList.add("canvas-footer");
      // Body element
      let body = document.createElement("div");
      body.classList.add("canvas-body", "canvas-position-sticky");
      /*               *Arrows elements*                 */
      let arrowSize = canvas.getAttribute("arrows-size") || "canvas-arrows-size-md";
      // Next element
      let NextEl = document.createElement("div");
      NextEl.classList.add("canvas-button-group", "canvas-right-0");
      // NextIcon = document
      NextEl.setAttribute("id", "next-page");
      // Next button
      let NextBtn = document.createElement("i");
      NextBtn.classList.add("material-icons", "canvas-arrows", "canvas-next-button", arrowSize);
      NextBtn.textContent = "chevron_right";
      NextEl.append(NextBtn);
      // Prev element
      let PrevEl = document.createElement("div");
      PrevEl.classList.add("canvas-button-group", "canvas-left-0");
      PrevEl.setAttribute("id", "prev-page");
      // Prev button
      let PrevBtn = document.createElement("i");
      PrevBtn.classList.add("material-icons", "canvas-arrows", "canvas-prev-button", arrowSize);
      PrevBtn.textContent = "chevron_left";
      PrevEl.append(PrevBtn);

      // Range input
      let Range = document.createElement("input");
      Range.classList.add("canvas-range");
      Range.setAttribute("type", "range");
      Range.setAttribute("min", "1");
      Range.setAttribute("value", "1");
      // Zoom
      let Zoom = document.createElement("input");
      Zoom.classList.add("canvas-zoom");
      Zoom.setAttribute("type", "range");
      Zoom.setAttribute("value", "1");
      Zoom.setAttribute("step", "0.2");
      Zoom.setAttribute("min", "1");
      Zoom.setAttribute("max", "2");
      // Page count
      let PageCount = document.createElement("span");
      PageCount.classList.add("canvas-page-count");
      PageCount.setAttribute("id", "page-count");

      body.prepend(NextEl);
      body.prepend(PrevEl);
      foot.append(PageCount);
      foot.append(Range);
      foot.append(Zoom);
      // foot.innerHTML += "<input class='canvas-zoom' type='range' value='0' step='0.2' max='2'>";
      article.prepend(head);
      article.append(body);
      article.append(foot);
      body.append(canvas);

      // let page_count = article.querySelector("#page-count");
      let src = canvas.getAttribute("src");
      let PDFDOC = await pdfjsLib.getDocument(src).promise;
      PageCount.textContent = '1/'+PDFDOC._pdfInfo.numPages;
      // console.log(PDFDOC._pdfInfo.numPages);
      Range.setAttribute("max", PDFDOC._pdfInfo.numPages);
      renderPage(1, canvas, PDFDOC);

      // Events handle
      PrevEl.addEventListener("click", function () {
        let pageNum = parseInt(Range.value) - 1;
        Range.value = pageNum;
        if (pageNum < 1) return false;
        PageCount.textContent = pageNum + "/" + PDFDOC._pdfInfo.numPages;
        renderPage(pageNum, canvas, PDFDOC);
      });
      NextEl.addEventListener("click", function () {
        let pageNum = parseInt(Range.value) + 1;
        Range.value = pageNum;
        if (pageNum > PDFDOC.numPages) return false;
        PageCount.textContent = pageNum + "/" + PDFDOC._pdfInfo.numPages;
        renderPage(pageNum, canvas, PDFDOC);
      });
      Range.addEventListener("change", function () {
        let num = parseInt(Range.value);
        PageCount.textContent = num + "/" + PDFDOC._pdfInfo.numPages;
        renderPage(num, canvas, PDFDOC);
      });
      Zoom.addEventListener("input", function () {
        canvasZoom(canvas, Zoom);
      });
    }
  } catch (err) {}
}
const canvasZoom = function (canvas, input) {
  let zoom = input.value;
  canvas.style.transform = `scale(${zoom})`;
};
